# Partie Angular du projet de blog :

[Lien vers le projet backend](https://gitlab.com/m9686/blog-back)

## Présentation des fonctionnalités :

Il s'agit d'un projet de blog sans veritable thème(J'ai manqué d'imagination pour le coup).
On peut y déposer un article via la page "écrire un article".
Une liste d'article est propoée en page d'accueil, chancun d'entre eux sont consultables individuelement.
Lorsqu'ils sont affichés dans leurs pages individuels suivis de leur commentaires(s'ils en ont).
On peut poster un commentaire sous chaque article mais également le supprimmer ou le mettre à jour.

## Structure de l'application :

- Trois entités : 
    - Article
    - Comment
    - menu(Cette dernière représente les éléments de la barre de navigation)
  
- Deux fichiers service qui servent à communiquer avec le serveur.
- Un composant navbar pour la barre de navigation.
- Un composant home qui affiche une liste de tous les articles présents dans la base.
- Deux composant form-page et update-page qui contienent des formulaire pour créer un nouvelle article ou en modifier un.
- Trois composants added-page, deleted-page et updated-page, vers lesquel l'utilisateur sera redirigés en cas de création,de suppression ou de mise à jours d'article, afin de les informer de la réussite de l'opération(Même s'il n'y a pas vraiment de contrôles...).
- Un composant article qui représente un article unique suivi des commentaires qui lui sont liés.

## Remarques personnelles :

- Je regrette de ne pas avoir fait un seul composant pour les added-page, deleted-page et updated-page. Ils sont très semblables, et j'aurais sûrement pu le modifier en passant une ActivatedRoute, mais j'y ai pensé vers la fin du projet. 
- Globalement, j'aurais certainement faire quelque-chose de plus maintenable.

## Maquettes :

[Dossier des Maquettes](https://gitlab.com/m9686/blogefrontend/-/tree/main/src/assets/Maquettes)

