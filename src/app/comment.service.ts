import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comment  } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http:HttpClient) { }

  post(comment:Comment):Observable<Comment> {
    return this.http.post<Comment>('http://localhost:8080/comment', comment)
  } 

  getAll():Observable<Comment[]> {
    return this.http.get<Comment[]>('http://localhost:8080/comment');
  }

  getById(id:number):Observable<Comment> {
    return this.http.get<Comment>('http://localhost:8080/comment/' + id);
  }

  getByArticleId(idArticle:number):Observable<Comment[]> {
    return this.http.get<Comment[]>('http://localhost:8080/comment/article/' + idArticle);
  }

  update(comment:Comment):Observable<Comment> {
    return this.http.put<Comment>('http://localhost:8080omment/' + comment.id, comment);
  }

  patch(comment:Comment):Observable<Comment> {
    return this.http.patch<Comment>('http://localhost:8080/comment/' + comment.id, comment);
  }

  delete(id:number) {
    return this.http.delete('http://localhost:8080/comment/' + id);
  }
}
