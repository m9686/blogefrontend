import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormPageComponent } from './form-page/form-page.component';
import { ArticleComponent } from './article/article.component';
import { AddedPageComponent } from './added-page/added-page.component';
import { DeletedPageComponent } from './deleted-page/deleted-page.component';
import { UpdatePageComponent } from './update-page/update-page.component';
import { UpdatedPageComponent } from './updated-page/updated-page.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FormPageComponent,
    ArticleComponent,
    AddedPageComponent,
    DeletedPageComponent,
    UpdatePageComponent,
    UpdatedPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
