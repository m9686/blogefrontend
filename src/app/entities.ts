export class Article {
  id?:number;
  title:String;
  author:String;
  content:String;
  date?:string;
  lisComment?:Comment[];

  constructor(title:String,author:String,content:String,date?:string,listComment?:Comment[],id?:number){
    this.id = id;
    this.title = title;
    this.author = author;
    this.content = content;
    this.date = date;
    this.lisComment = listComment;
  }
}

export class Comment {
  id?:number;
  id_article:number;
  author:String;
  content:String;
  date?:Date;

  constructor(id_article:number,author:String,content:String,date?:Date,id?:number,) {
    this.id = id;
    this.id_article = id_article;
    this.author = author;
    this.content = content;
    this.date = date;
  }
} 

export interface menu {
  label:string;
  url:string;
}