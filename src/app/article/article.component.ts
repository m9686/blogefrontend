import { Component, OnInit } from '@angular/core';
import { Article } from '../entities';
import { ArticleService } from '../article.service';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from '../comment.service';
import { Comment } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  constructor(private as:ArticleService, private cs:CommentService,  private route:ActivatedRoute) { }

  article?:Article;
  routeId?:string;
  toggle:boolean = false;

  author:string = "";
  content:string = "";

  badAuthor = false;
  badContent = false;

  check() {
    if (this.badAuthor == true || this.badContent == true) {
      return false;
    }
    return true;
  }

  changeState(author:string, content:string) {
    if (author.length <= 2) {
      this.badAuthor = true;
    } else {
      this.badAuthor = false;
    }
    if (content == "") {
      this.badContent = true;
    } else {
      this.badContent = false;
    }
  }

  addComment():void {
    this.changeState(this.author, this.content)
    if (this.check()) {
      let newComment:Comment = new Comment(this.article!.id!, this.author!, this.content!, new Date);
      this.cs.post(newComment).subscribe();
      this.article?.lisComment?.push(newComment);
      this.author = "";
      this.content = "";
      this.toggler();
    }
  }

  deleteComment(id:number):void {
    this.cs.delete(id).subscribe();
    this.deleteInCommentList(id);
  }
  deleteInCommentList(id:number):void {
    this.article?.lisComment?.forEach(e => {
      if (e.id == id) {
        this.article?.lisComment?.splice(this.article?.lisComment?.indexOf(e), 1);
      }
    })
  }

  deleteArticle(id:number) {
    this.as.delete(id).subscribe();
  }

  toggler() {
    this.toggle = !this.toggle;
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    this.as.getById(Number(this.routeId)).subscribe(data => this.article = data);
  }

}
