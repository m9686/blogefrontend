import { Component, OnInit } from '@angular/core';
import { menu } from '../entities';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  toggle:boolean = false;

  pagesList:menu[] = [
    {label:"🏠Accueil", url:''},
    {label:"✍🏻Poster", url:'new-article'}, 
  ]

  toggler() {
    this.toggle = !this.toggle;
  }

  ngOnInit(): void {
  }

}
