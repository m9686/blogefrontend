import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormPageComponent } from './form-page/form-page.component';
import { ArticleComponent } from './article/article.component';
import { AddedPageComponent } from './added-page/added-page.component';
import { DeletedPageComponent } from './deleted-page/deleted-page.component';
import { UpdatePageComponent } from './update-page/update-page.component';
import { UpdatedPageComponent } from './updated-page/updated-page.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new-article', component:FormPageComponent},
  {path: 'article/:id', component:ArticleComponent},
  {path: 'added', component:AddedPageComponent},
  {path: 'article/:id/deleted', component:DeletedPageComponent},
  {path: 'article/:id/update', component:UpdatePageComponent},
  {path: 'updated', component:UpdatedPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
