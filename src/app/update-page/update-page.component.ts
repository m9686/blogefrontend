import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-update-page',
  templateUrl: './update-page.component.html',
  styleUrls: ['./update-page.component.css']
})
export class UpdatePageComponent implements OnInit {

  constructor(private as:ArticleService, private router:Router,  private route: ActivatedRoute) { }
  
  article:Article = new Article("", "", "");

  routeId:number = 0;

  badTitle = false;
  badAuthor = false;
  badContent = false;

  send() {
    this.changeState(this.article)
    if (this.check()) {
    this.as.update(this.article).subscribe( () => {
      this.router.navigate(['/updated']);
    });
  }
}

  check():boolean {
    if (this.badTitle == true || this.badAuthor == true || this.badContent == true) {
      return false;
    }
    return true;
  }

  changeState(article:Article) {
    if (article.title.length <= 2) {
      this.badTitle = true;
    } else {
      this.badTitle = false;
    }
    if (article.author.length <= 2) {
      this.badAuthor = true;
    } else {
      this.badAuthor = false;
    }
    if (article.content == "") {
      this.badContent = true;
    } else {
      this.badContent = false;
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    this.as.getById(Number(this.routeId)).subscribe(data => this.article = data);
  }

}
