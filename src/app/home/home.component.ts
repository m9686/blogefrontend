import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { CommentService } from '../comment.service';
import { Article } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private as: ArticleService, private cs:CommentService) { }

  articles?:Article[];

  ngOnInit(): void {
    this.as.getAll().subscribe(data => this.articles = data);
  }

}
