import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.css']
})
export class FormPageComponent implements OnInit {

  constructor(private as:ArticleService, private router:Router) { }

  article:Article = new Article("", "", "");

  badTitle = false;
  badAuthor = false;
  badContent = false;

  send() {
    this.changeState(this.article)
    if (this.check()) {
    this.as.post(this.article).subscribe(  () => {
      this.router.navigate(['/added']);
    });
    }
  }
  
  check():boolean {
    if (this.badTitle == true || this.badAuthor == true || this.badContent == true) {
      return false;
    }
    return true;
  }

  changeState(article:Article) {
    if (article.title.length <= 2) {
      this.badTitle = true;
    } else {
      this.badTitle = false;
    }
    if (article.author.length <= 2) {
      this.badAuthor = true;
    } else {
      this.badAuthor = false;
    }
    if (article.content == "") {
      this.badContent = true;
    } else {
      this.badContent = false;
    }
  }

  ngOnInit(): void {
  }

}
