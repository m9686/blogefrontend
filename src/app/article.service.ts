import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError, retry } from 'rxjs';
import { Article } from './entities';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http : HttpClient) { }

  post(article:Article):Observable<Article> {
    return this.http.post<Article>('http://localhost:8080/article', article)
  } 

  getAll():Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/article');
  }

  getById(id:number):Observable<Article> {
    return this.http.get<Article>('http://localhost:8080/article/' + id);
  }

  update(article:Article):Observable<Article> {
    return this.http.put<Article>('http://localhost:8080/article/' + article.id, article);
  }

  patch(article:Article):Observable<Article> {
    return this.http.patch<Article>('http://localhost:8080/article/' + article.id, article);
  }

  delete(id:number) {
   return this.http.delete('http://localhost:8080/article/' + id);
  }
}